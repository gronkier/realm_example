package com.example.rc.samples.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.example.rc.samples.model.Person.ID;
import static com.example.rc.samples.model.Person.NAME;
import static com.example.rc.samples.model.Person.PHONE;
import static com.example.rc.samples.model.Person.RATING;
import static com.example.rc.samples.model.Person.TABLE;

/**
 * Created by RaVxp on 05.05.2017.
 */

public class PersonSQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "commments.db";

    private static final int DATABASE_VERSION = 4;

    private static final String DATABASE_CREATE =
            "create table "
                    + TABLE + "( "
                    + ID + " integer primary key autoincrement, "
                    + NAME + " text not null,"
                    + RATING + " real not null,"
                    + PHONE + " text not null"
                    + ");";

    public static final String DATABASE_DROP = "DROP TABLE IF EXISTS " + TABLE;

    public PersonSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DATABASE_DROP);
        onCreate(sqLiteDatabase);
    }
}
