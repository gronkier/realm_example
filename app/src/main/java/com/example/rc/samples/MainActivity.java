package com.example.rc.samples;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.rc.samples.adapters.PersonAdapter;
import com.example.rc.samples.model.Person;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.emptyListView)
    protected View emptyView;

    @BindView(R.id.list)
    protected ListView list;


    private PersonAdapter adapter;
    Realm realm = Realm.getDefaultInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        initList();

    }

    private void initList() {
        list.setEmptyView(emptyView);

        addContent();
        final RealmResults all = realm.where(Person.class).findAll();



        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Person person = realm.createObject(Person.class);
                person.setName("Jacek");
                person.setPhone("142318");
                person.setRating((float) 3.0);
            }
        });
        adapter = new PersonAdapter(this, all);
        list.setAdapter(adapter);
        registerForContextMenu(list);
        all.addChangeListener(new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                adapter.notifyDataSetChanged();
            }
        });
        adapter.setOnRatingChangeListener(new PersonAdapter.OnRatingChangeListener() {
            @Override
            public void onChange(final Person person, final float rating) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        person.setRating(rating);
                    }
                });
            }
        });

    }


    private void addContent() {

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Person.class).findAll().deleteAllFromRealm();
                for (int x = 0; x < 10; ++x) {
                    Person person = realm.createObject(Person.class);
                    person.setName("Janek" + x);
                    person.setPhone("2437897234");
                    person.setRating((float) x % 5 + 1);
                }
            }
        });

    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.delete:
                final Person person = adapter.getItem(info.position);
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        person.deleteFromRealm();;
                    }
                });
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }


}
