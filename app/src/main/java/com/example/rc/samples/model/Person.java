package com.example.rc.samples.model;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by RaVxp on 05.05.2017.
 */

public class Person extends RealmObject {
    public Person() {
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public static final String TABLE = "people";

    public static final String ID = "_id";

    public static final String NAME = "name";

    public static final String RATING = "rating";

    public static final String PHONE = "phone";

    private String name;

    private Float rating;

    private String phone;

    public Person(String name, Float rating, String phone) {
        this.name = name;
        this.rating = rating;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public Float getRating() {
        return rating;
    }

    public String getPhone() {
        return phone;
    }
}
