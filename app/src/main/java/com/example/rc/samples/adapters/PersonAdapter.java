package com.example.rc.samples.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.rc.samples.R;
import com.example.rc.samples.model.Person;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;


public class PersonAdapter extends ArrayAdapter<Person> {

    @BindView(R.id.name)
    protected TextView name;

    @BindView(R.id.rating)
    protected RatingBar rating;

    private OnRatingChangeListener onRatingChangeListener = new OnRatingChangeListener() {
        @Override
        public void onChange(Person model, float rating) {

        }


    };

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Person model = getItem(position);
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.person_row, parent, false);
            convertView.setLongClickable(true);
        }
        ButterKnife.bind(this, convertView);
        name.setText(model.getName());
        rating.setRating(model.getRating());

        rating.setTag(model);
        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                if (b) {
                    onRatingChangeListener.onChange((Person) ratingBar.getTag(), v);
                }
            }
        });
        return convertView;
    }

    public PersonAdapter(Context context, RealmResults<Person> persons) {
        super(context, 0,persons);
    }



    public interface OnRatingChangeListener {
        void onChange(Person model, float rating);
    }

    public void setOnRatingChangeListener(OnRatingChangeListener onRatingChangeListener) {
        this.onRatingChangeListener = onRatingChangeListener;
    }
}
