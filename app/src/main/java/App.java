import android.app.Application;

import io.realm.Realm;

/**
 * Created by RENT on 2017-05-16.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
